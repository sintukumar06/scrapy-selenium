# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

from scrapy import Item, Field
from scrapy.loader.processors import MapCompose, TakeFirst


def get_id(item):
    return (item.split(":")[1]).strip()


def split_item(item):
    return item.split(",")


def split_item_by_space(item):
    return item.strip().split(" ")


class WebItem(Item):
    name = Field(
        input_processor=MapCompose(str.strip),
        output_processor=TakeFirst()
    )
    latitude = Field(
        output_processor=TakeFirst()
    )
    longitude = Field(
        output_processor=TakeFirst()
    )
    id = Field(
        input_processor=MapCompose(get_id),
        output_processor=TakeFirst()
    )
    address = Field(
        input_processor=MapCompose(split_item)
    )
    images = Field()
    price = Field(
        output_processor=TakeFirst()
    )
    type = Field(
        input_processor=MapCompose(split_item_by_space),
        output_processor=TakeFirst()
    )
    description = Field(
        output_processor=TakeFirst()
    )
