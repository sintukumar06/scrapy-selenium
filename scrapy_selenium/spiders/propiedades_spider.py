import scrapy
from scrapy.loader import ItemLoader

from scrapy_selenium.items import WebItem


class propiedades_spider(scrapy.Spider):
    name = 'propiedades'
    base_url = 'https://propiedades.com'
    allowed_domains = ['propiedades.com']
    start_urls = ['https://propiedades.com/campeche']

    def parse(self, response):
        for item in response.xpath('.//div[contains (@class, "properties-list")]'):
            loader = ItemLoader(item=WebItem(), selector=item)
            loader.add_xpath('latitude', './/div[@itemprop="geo"]/meta[@itemprop="latitude"]/@content')
            loader.add_xpath('longitude', './/div[@itemprop="geo"]/meta[@itemprop="longitude"]/@content')
            web_item = loader.load_item()
            property_url = item.xpath(".//p[@class='title-property']/a").attrib['href']
            yield response.follow(property_url, self.parse_property, meta={'web_item': web_item})
            break

    def parse_property(self, response):
        images_string = ''
        web_item = response.meta['web_item']
        loader = ItemLoader(item=web_item, response=response)
        loader.add_xpath('name', ".//h1[@class='title-gallery']/em/text()")
        loader.add_css('price', 'span.price-gallery::text')
        loader.add_xpath('type', ".//div[@itemprop='address']/p/text()")
        loader.add_xpath('id', './/span/strong/text()')
        loader.add_xpath('address', '//*[@id="search-nu-input"]/@value')
        loader.add_xpath('description', '//*[@id="nav-details"]/div[2]/p[2]/text()')
        for image in response.xpath(".//meta[@itemprop='image']/@content"):
            images_string += image.get() + ";"
        loader.add_value('images', images_string)

        yield loader.load_item()
