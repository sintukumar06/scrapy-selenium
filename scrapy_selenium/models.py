from scrapy.utils.project import get_project_settings
from sqlalchemy import create_engine, Column, Integer, Text
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(get_project_settings().get("CONNECTION_STRING"))


def create_table(engine):
    Base.metadata.create_all(engine)


class WebItem(Base):
    __tablename__ = "webitem"

    id = Column(Integer, primary_key=True)
    unit_name = Column('unit_name', Text())
    bathrooms = Column('bathrooms', Text())
    bedrooms = Column('bedrooms', Text())
    created = Column('created', Text())
    url_site = Column('url_site', Text())
    latitude = Column('latitude', Text())
    longitude = Column('longitude', Text())
    unit_url = Column('unit_url', Text())
    unit_id = Column('unit_id', Text())
    unit_type = Column('unit_type', Text())
    price = Column('price', Text())
    unit_price = Column('unit_price', Text())
    land_size = Column('land_size', Text())
    unit_land_size = Column('unit_land_size', Text())
    parking_lot = Column('parking_lot', Text())
    construction_size = Column('construction_size', Text())
    unit_construction_size = Column('unit_construction_size', Text())
    site_name = Column('site_name', Text())
    url_lmages = Column('url_lmages', Text())
    municipio = Column('municipio', Text())
    colonia = Column('colonia', Text())
    type = Column('type', Text())
    description = Column('description', Text())
    estado = Column('estado', Text())
    updated_date = Column('updated_date', Text())
