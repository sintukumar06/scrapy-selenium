# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html
import logging

from scrapy.exceptions import DropItem
from sqlalchemy.orm import sessionmaker

from scrapy_selenium.models import db_connect, create_table, WebItem


class DuplicatesPipeline(object):

    def __init__(self):
        """
        Initializes database connection and sessionmaker.
        Creates tables.
        """
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)
        logging.info("****DuplicatesPipeline: database connected****")

    def process_item(self, item, spider):
        session = self.Session()
        exist_address = session.query(WebItem).filter_by(unit_name=item['name']).first()
        if exist_address is not None:  # the current quote exists
            session.close()
            raise DropItem("Duplicate item found: %s" % item["quote_content"])
        else:
            session.close()
            return item


class WebItemPipeline(object):
    def __init__(self):
        """
        Initializes database connection and sessionmaker
        Creates tables
        """
        engine = db_connect()
        create_table(engine)
        self.Session = sessionmaker(bind=engine)

    def process_item(self, item, spider):
        session = self.Session()
        web_item = WebItem()
        web_item.unit_name = item['name']
        web_item.longitude = item['longitude']
        web_item.latitude = item['latitude']
        web_item.unit_id = item['id']
        web_item.unit_type = item['id'][-1]
        web_item.type = item['type'][0]
        web_item.url_lmages = str(item['images'])
        web_item.description = item['description']
        web_item.municipio = item['address'][0]
        web_item.colonia = item['address'][1]
        web_item.estado = item['address'][2]

        try:
            session.add(web_item)
            session.commit()

        except:
            session.rollback()
            raise

        finally:
            session.close()

        return item
